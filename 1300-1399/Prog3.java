import java.util.Scanner;

public class Prog3 {
   public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        String num1 = sc.next();
        String num2 = sc.next();
        int n = num1.length();

        StringBuilder sb = new StringBuilder(n);

        for(int i=0;i<n;i++){
            if(num1.charAt(i)==num2.charAt(i)){
                sb.append('0');
            }
            else{
                sb.append('1');
            }
        }

        System.out.println(sb.toString());
   } 
}
