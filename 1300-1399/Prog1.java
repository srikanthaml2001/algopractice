import java.util.Scanner;

public class Prog1{
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int [] arr = new int[n];
        for(int i=0;i<n;i++){
            arr[i] = sc.nextInt();
        }
        int maxValue=0,minValue=100,maxIndex=0,minIndex=0;
        for(int i=0;i<n;i++){
            if(arr[i]<=minValue){
                minValue = arr[i];
                minIndex = i;
            }
            if(arr[i]>maxValue){
                maxValue = arr[i];
                maxIndex = i;
            }
        }
        if(minIndex<maxIndex){
            System.out.println(maxIndex + (n-1) - minIndex - 1);
        }
        else if(minIndex>maxIndex){
            System.out.println(maxIndex + (n-1) - minIndex);
        }
        else{
            System.out.println(0);
        }
    }
}