## List of Problems


|Sl.no  | Problem Name | Solution Link |
|-------|--------------|---------------|
|1|[Arrival of the General](https://codeforces.com/problemset/problem/144/A)|[Solution](./1300-1399/Prog1.java)|
|2|[Beautiful Matrix](https://codeforces.com/problemset/problem/263/A)|[Solution](./1300-1399/Prog2.java)|